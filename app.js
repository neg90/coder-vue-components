import { createApp } from 'https://unpkg.com/vue@3/dist/vue.esm-browser.js'

import tableComponent from './components/table.js'

createApp({

    components: {
        tableComponent
    },

    data: () => ({
        table_prop: [
            {
                'title': 'Male employers',
                'background': 'orange',
                'font_size': '1.2rem'
            },
            {
                'title': 'Female employers',
                'background': 'red',
                'font_size': '1.2rem'
            },
            {
                'title': 'Pets',
                'background': 'green',
                'font_size': '1rem'
            }
        ],
        tables: [
            [
                {
                    'name': 'Miguel Ángel',
                    'contact': 'miguelangel@empresa.com',
                    'country': 'Germany'
                },
                {
                    'name': 'Juan Carlos',
                    'contact': 'juancarlos@empresa.com',
                    'country': 'Mexico'
                },
                {
                    'name': 'Carlos Alberto',
                    'contact': 'carlosalberto@empresa.com',
                    'country': 'Argentina'
                }
            ],
            [
                {
                    'name': 'Crésida Mendez',
                    'contact': 'cresidamendez@empresa.com',
                    'country': 'Germany'
                },
                {
                    'name': 'Adara Perez',
                    'contact': 'adaraperez@empresa.com',
                    'country': 'Mexico'
                },
                {
                    'name': 'Julieta Gonzales',
                    'contact': 'julietagonzales@empresa.com',
                    'country': 'Argentina'
                }
            ],
            [
                {
                    'name': 'Toto',
                    'contact': '-',
                    'country': 'Germany'
                },
                {
                    'name': 'Ninja',
                    'contact': '-',
                    'country': 'Mexico'
                },
                {
                    'name': 'Luke',
                    'contact': '-',
                    'country': 'Argentina'
                }
            ],
        ]
    })
}).mount('#app')