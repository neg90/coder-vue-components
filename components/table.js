export default {

    props: {
        employers: Array,
        background: String,
        font_size: String,
        title: String
    },

    template:
        `<div class="table-container">
            <h4> {{title}} </h4>
            <table :style="'background-color: ' + background + '; font-size: ' + font_size">
                <tr>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>Country</th>
                </tr>
                <tr v-for="(employer, e) in employers">
                    <td>{{employer.name}}</td>
                    <td>{{employer.contact}}</td>
                    <td>{{employer.country}}</td>
                </tr>
            </table>
        </div>`
}
